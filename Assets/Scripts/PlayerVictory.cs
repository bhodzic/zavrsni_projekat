﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerVictory : MonoBehaviour
{
    private BoxCollider2D coll;
    private GameModel model;
    // Start is called before the first frame update
    void Start()
    {
        model = UnityEngine.Object.FindObjectOfType<GameModel>();
        coll = GetComponent<BoxCollider2D>();
        coll.isTrigger = true;
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            Time.timeScale = 0;
            model.mainMenu.gameObject.SetActive(true);
            model.gameWon();
        }
    }
}
