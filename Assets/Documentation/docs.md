# ZAVRŠNI PROJEKAT - BENJAMIN HODŽIĆ

## Tehnička dokumentacija za završni projekat

### Ideja

Za svoj završni rad sam izabrao put game developmenta. Pošto je moćan komercijalni game engine rijetkost (Unity, Unreal), početnik nema puno izbora. Pri tome, raditi prvi projekat u 3 dimenzije je prevelik zalogaj za početnike, stoga sam izabrao 2D platformer igricu.

U pitanju je "kombinacija" između [Gravity guy](https://en.wikipedia.org/wiki/Gravity_Guy), gdje korisnik voljno mijenja smjer gravitacije, i kultne igrice [Super Mario World](https://en.wikipedia.org/wiki/Super_Mario_World) sa SNES konozole iz 90-ih godina prošlog stoljeća.

Može se reći da je igrica koju sam razvio _cross-platform_, jer Unity framework podržava kompajliranje u produkcijski build u samo par klikova.

### Realizacija

Unity koristi pouzdani objektno-orjentisani pristup pri razvijanju igara. Svaka komponenta igre je zapravo klasa _GameObject_. Njome se može predstaviti sve od običnog _Sprite_ objekta, do kamere koja prati igrača.

![GameObject klase](GameObjects.png)

Svaka instanca GameObject se sastoji od proizvoljno mnogo komponenata. Te komponente mogu biti _Collider_ objekti, zglobovi za kretanje, izvori svjetlosti, izvori zvuka, itd.

Najkorisnija komponenta za manipulaciju _GameObject-a_ je skripta.
Unity koristi C# skripte kako bi game developeri mogli upravljati komponentama igre uprkos veoma moćnom korisničkom interfejsu.

![UI](UI.png)

Sama skripta nasljeđuje klasu _MonoBehaviour_. Koncept je vrlo sličan kao u Android programiranjul. _MonoBehaviour_ ima nešto poput lifecycle metoda, npr:

```
void Start() {}

void Update() {}
```

`Start()` se poziva **samo** jednom prilikom inicijaliziranja svih objekata. `Update()` se poziva svaki frame.

U ovom projektu je raspored klasa "centralizovan". Sva logika je raspoređena na nekoliko skripti, međutim svakoj od njih se može pristupiti preko _GameModel_ skripte.
To je korisno jer je pretraga po hijerarhijskom stablu objekata veoma skupa i treba da se radi upravo u gore navedenoj funkciji `Start()`.

### Igra

Igrač se kreće kroz level, klikom na space mijenja se gravitacija igrača. Ako igrač ode u zonu iz koje se ne može vratiti, igra se automatski resetuje, te se sprite postavlja na početni položaj.

Kraj igre se provjerava preko _collision detection_, kao i mnoge druge stvari u igri. Npr provjera da li je igrač pobijedio, tj. došao do kraja, skupljanje gemova.

Igrač može skupljati gemove krećući se kroz nivo, pri čemu gemovi nestaju. Ukoliko igrač propadne, svi gemovi će se resetovati.

![Gemovi](gems.png)

Također je implementiran takozvani _Parallax sloj_, pomjerajući pozadinski sloj dobija se efekat da se igrač doista kreće.

Klikom na _Escape_ otvara se meni, koji je implementiran preko _Canvas_ objekta. Meni se sastoji od naslova i interaktivnih dugmića koji omogućavaju korisniku da restartuje igru, skroz napusti aplikaciju ili nastavi s nivoom.

![Meni](meni.png)

Svi grafički resursi su skinuti sa interneta, besplatno. To uključuje pozadinu, blokove na nivou levela, sprite za igrača.

Pozadina je urađena kroz _Tile palette_. Matrica kockica u koje se smještaju razne strukture, a iz njih direktno crtanje na podlogu.

![Tile palette](tile_palette.png)

Što se tiče sprite resursa, vrlo je jednostavno ubaciti ih u projekat. Potrebno je ubaciti sliku u _Assets_ folder i izabrati "Sprite (2D and UI)" pod tip teksture. Ako na jednoj slici ima više sprite resursa, postoji _Sprite editor_ u kojem se mogu pojedinačno ubaciti.

![Sprite](sprite_inspector.png)
![Sprite editor](sprite_editor.png)

### Collision detection

Vrijedi spomenuti stvar koja se dosta koristi, a dostupna je direktno iz Unity game engine-a.
Naime, svaki put kada igrač pokupi gem, kada propadne i kada pokori nivo, vrši se provjera _collision detection_.
Igrač, koji je tipa **Rigidbody2D**, koji uz sebe ima "zakačen" BoxCollider2D instancu, ima kontinuiranu provjeru kolizije među **svim** objektima koji su tipa **Collider**. To znači da game engine pri svakom frejmu provjerava da li je došlo do kolizije.

Na svaki objekat koji na sebi ima neki _collider_ možemo definisati metodu, koja je zapravo listener

```
void OnCollisionEnter2D(Collision2D collision);
void OnTriggerEnter2D(Collide2D collider);
```

Razlika između ove dvije metode je da neki objekti koji su _collideri_, mogu se ponašati kao triggeri. Upravo ti triggeri su gemovi i zone u kojima igrač propada.
Jedini "pravi" collideri, koji su od kompozitnog materijala i zaista izazivaju mehaničke sudare, su sami igrač (sprite) i podloga po kojoj se igrač kreće (Tilemap Collider 2D)
