﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class GameModel : MonoBehaviour
{
    public Cinemachine.CinemachineVirtualCamera cam;
    private bool running = true;
    private bool victory = false;
    public PlayerController player;
    private GameObject playerObject;
    public Camera mainCam;
    public UIController mainMenu;
    private GemController gemController;
    private TMPro.TextMeshProUGUI heading;
    [ShowOnly]
    public int collectedGems = 0;

    void Start()
    {
        gemController = GetComponent<GemController>();
        cam = GameObject.Find("MainCamera").GetComponent<Cinemachine.CinemachineVirtualCamera>();
        playerObject = GameObject.Find("Player");
        player = playerObject.GetComponent<PlayerController>();
        mainMenu.gameObject.SetActive(false);
    }

    public IEnumerator GameOver()
    {
        cam.m_Follow = null;
        cam.m_LookAt = null;
        yield return new WaitForSeconds(2f);
        ResetGame();
    }

    public void ResetGame()
    {
        player.transform.position = new Vector2(-17, 10);
        player.Initialize();
        gemController.ResetGems();
        heading.SetText("Zavrsni rad");
        collectedGems = 0;
        cam.m_Follow = player.transform;
        cam.m_LookAt = player.transform;
    }

    public void ResumeGame() {
        Time.timeScale = 1;
        mainMenu.gameObject.SetActive(false);
    }

    void Update()
    {
        if (heading == null) {
            heading = mainMenu.transform.GetChild(0).GetChild(0).GetComponent<TMPro.TextMeshProUGUI>();
        }
        if (Input.GetKeyUp(KeyCode.Escape))
        {
            if (running)
            {
                Time.timeScale = 0;
                mainMenu.gameObject.SetActive(true);
            }
            else
            {
                ResumeGame();
            }
            running = !running;
        }
    }

    public void IncrementGems() {
        collectedGems++;
    }

    public void gameWon() {
        victory = true;
        heading.SetText("VICTORY");
    }
}
