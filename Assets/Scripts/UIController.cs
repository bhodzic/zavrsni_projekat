﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIController : MonoBehaviour
{
    private GameModel model;
    void Start()
    {
        model = UnityEngine.Object.FindObjectOfType<GameModel>();
        gameObject.SetActive(false);
    }

    public void OnClickButton(int id)
    {
        if (id == 1)
        { // RESUME DUGME
            model.ResumeGame();
        }
        else if (id == 2)
        { // RESTART DUGME
            model.ResetGame();
            model.ResumeGame();
        }
        else if (id == 3)
        { // QUIT DUGME
            Application.Quit();
        }
    }
}
