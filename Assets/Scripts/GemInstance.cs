﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemInstance : MonoBehaviour
{
    private CircleCollider2D coll;
    private GameModel model;
    // Start is called before the first frame update
    private bool active = true;
    void Start()
    {
        GameModel m = (GameModel)FindObjectOfType(typeof(GameModel));
        model = m.GetComponent<GameModel>();
        coll = GetComponent<CircleCollider2D>();
        coll.isTrigger = true;
        Transform t = GetComponent<Transform>();
        t.position = new Vector3(t.position.x, t.position.y, 0);
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    void OnTriggerEnter2D(Collider2D collider) {
        if (collider.gameObject.tag == "Player") {
            active = false;
            model.IncrementGems();
        }
    }

    public bool getActive() => active;

    public void setActive() {
        active = true;
    }
}
