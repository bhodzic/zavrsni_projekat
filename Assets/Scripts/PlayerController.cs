﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    private bool orientation = false;
    public VerticalState yState;
    public float verticalVelocity = 1f;
    public float horizontalVelocity = 0.4f;
    protected Rigidbody2D body;
    private SpriteRenderer r;
    // Start is called before the first frame update
    void Start()
    {
        body = GetComponent<Rigidbody2D>();
        body.freezeRotation = true;
        r = GetComponent<SpriteRenderer>();
        Initialize();
    }

    // Update is called once per frame
    void Update()
    {
        Vector2 velocity = body.velocity;
        // if (yState == VerticalState.Started && velocity.y == 0)
        // { // cekati da sprite padne, nakon toga automatski se krece desno
        //     horizontalVelocity = 0.4f;
        //     yState = VerticalState.Grounded;
        // }


        if (Input.GetKeyUp(KeyCode.Space) && yState == VerticalState.Grounded)
        {
            orientation = !orientation;
            if (orientation) {
                r.flipY = true;
            } else {
                r.flipY = false;
            }
            yState = VerticalState.InFlight;
            body.gravityScale = 0;
        }

        if (yState == VerticalState.InFlight)
        {
            if (orientation)
            {
                transform.Translate(new Vector3(horizontalVelocity, verticalVelocity, 0) * 10 * Time.deltaTime);
            }
            else
            {
                transform.Translate(new Vector3(horizontalVelocity, -verticalVelocity, 0) * 10 * Time.deltaTime);
            }
            return;
        }
        transform.Translate(new Vector3(horizontalVelocity, 0, 0) * 10 * Time.deltaTime);
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        ContactPoint2D[] contacts = new ContactPoint2D[10];
        collision.GetContacts(contacts);
        string tag = collision.gameObject.tag;
        bool isSideCollision = contacts[0].normal.x != 0;
        if (tag == "Platform" && collision.contacts[0].normal == Vector2.up && yState == VerticalState.Started)
        { // cekati da sprite padne, nakon toga automatski se krece desno
            horizontalVelocity = 0.4f;
            yState = VerticalState.Grounded;
            body.gravityScale = 0;
        }
    }

    void OnCollisionStay2D(Collision2D collision)
    {
        ContactPoint2D[] contacts = new ContactPoint2D[10];
        collision.GetContacts(contacts);
        foreach (ContactPoint2D c in contacts)
        {
            if ((c.normal == Vector2.down && orientation) || (c.normal == Vector2.up && !orientation) && yState == VerticalState.InFlight)
            { // doslo je do bocne, zatim do vertikalne kolizije
                yState = VerticalState.Grounded;
            }
        }
    }

    void OnCollisionExit2D(Collision2D collision)
    {
        if (yState == VerticalState.Grounded)
        {
            yState = VerticalState.InFlight;
        }
    }

    public void Initialize()
    {
        Physics2D.gravity = new Vector2(0, -9.87f);
        horizontalVelocity = 0f;
        body.gravityScale = 1;
        r.flipY = false;
        yState = VerticalState.Started;
        orientation = false;
    }

    public enum VerticalState
    {
        Grounded,
        Started,
        InFlight
    }
}
