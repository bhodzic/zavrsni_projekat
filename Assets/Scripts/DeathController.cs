﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DeathController : MonoBehaviour
{
    protected BoxCollider2D zone;
    protected GameModel model;
    // Start is called before the first frame update
    void Start() {
        zone = GetComponent<BoxCollider2D>();
        zone.isTrigger = true;
        model = UnityEngine.Object.FindObjectOfType<GameModel>();
    }

    void OnTriggerEnter2D(Collider2D collision) {
        if (collision.gameObject.tag == "Player") {
            // resetovati gemove, vratiti igraca na pocetak, "otkaciti kameru"
            StartCoroutine(model.GameOver());
        }
    }
}
