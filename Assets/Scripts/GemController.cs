﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GemController : MonoBehaviour
{
    private GemInstance[] gems;
    // Start is called before the first frame update
    void Start()
    {
        gems = UnityEngine.Object.FindObjectsOfType<GemInstance>();
    }

    // Update is called once per frame
    void Update()
    {
        foreach (GemInstance gem in gems) {
            if (!gem.getActive()) {
                gem.gameObject.SetActive(false);
            }
        }
    }

    public void ResetGems() {
        foreach (GemInstance gem in gems) {
            gem.setActive();
            gem.gameObject.SetActive(true);
        }
    }
}
